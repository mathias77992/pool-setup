# Cryptocurrency Mining Pool Setup Script

#### Usage:

**`sudo bash -c "$(curl -ko - https://gitlab.com/sehidcan/pool-setup/raw/master/poolsetup)"`**

or

**`sudo bash -c "$(wget --no-check-certificate -O - https://gitlab.com/sehidcan/pool-setup/raw/master/poolsetup)"`**